---
layout: markdown_page
title: GDPR Requests
---

Under Article 15 of the GDPR, you have the right to request that GitLab provides you with information about the personal data GitLab has about you. You also have the right to request complete removal of your GitLab.com account in accordance with the GDPR. 

To submit these requests, please email `gdpr-request@gitlab.com`.

Please ensure that you:

- send the email from the address associated with your GitLab.com account
- include the GitLab username (if applicable)

Please note that:

- in many circumstances, we will not be able to provide a full export of associated repositories
- (if requesting account deletion) any groups that you're a sole owner in will be deleted, along with any projects contained therein

Once you've submitted your request, we'll have the various teams review our systems for your personal data and send you final confirmation after this process is complete.