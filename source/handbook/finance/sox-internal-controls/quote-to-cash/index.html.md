---
layout: handbook-page-toc
title: "Quote to Cash"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}




**Introduction**


## 1. Customer Account Management and Conversion of lead to opportunity
 
   To be updated

<br>

## 2. Price Master Management

   To be updated

  <br> 
   
## 3. Quote Creation

   To be updated
   
   <br>
   
## 4. Reseller Management

   To be updated
   
   <br>
   
## 5. Contract Management

   To be updated
   
   <br>
   
## 6. Invoicing to Customers 

   To be updated
   
   <br>
   
## 7. Invoice Cancellations and Refunds

   To be updated
   
   <br>
   
## 8. Revenue Recognition and Accounting for other quote to cash transactions in NetSuite

   To be updated
   
   <br>
   
## 9. Accounting of Income from sale of merchandise

   To be updated
   
   <br>
   
## 10. Accounting of Income from GCP Referral

   To be updated
   
   <br>
   
## 11. Accounts Receivable

   To be updated
   
   <br>
   
## 12. Commission payouts to Sales executives

   To be updated
   
   <br>
   
## 13. Licence Key

[Link to Flowchart](https://docs.google.com/presentation/d/1EHPu9_TI9lFUPek-0fD2WjWbP_devWR4y3tfR8PTgUE/edit)

There are two models in which customers can use the product:<br>
    1. On premise model i.e self managed<br>
    2. On cloud model

In case of a self managed model the customer downloads the products onto his/her server and manages the product. 

In the case of the on cloud model the product is hosted on GitLab server and is available to customers through the internet.



**1. On Premise Model**

Sales is made to customer through following channels:

**a. Web Direct Sales:**

Customer logs in to Customers Portal and directly buys the product as per his/her requirements.

* Step 1: Customer completes the purchase in Customers Portal and credit card details go through. An opportunity is automatically created in Salesforce.

* Step 2: Transaction is automatically pushed  to Zuora from Salesforce once purchase is successful.

* Step 3: Zuora pushes the customer information along with Zuora reference number to the [customer portal](https://docs.google.com/document/d/1iLiq3IegyaFZISDUAXcDVt87PzGS3zn8SKZrRvcJgYY/edit). 

* Step 4: Upon successful receipt of Zuora reference number the customer portal will push the details to the license application . Details such as customer info, contact details and product details are automatically pushed into the [license app](https://docs.google.com/document/d/1fSIewFE_uDiTyLolFC1CHbYAwTFDgGm25Wy_HWxY-pw/edit).

* Step 5: license application automatically sends license Key to customer through an email, alternatively customer can also download the license key through customer portal.


**b. Sales team assisted sales:**

Sales team is involved in the sales process, SalesForce is used to create customer accounts, send out quotes and close the deal.

* Step 1: Sales team completes the sales and marks the [opportunity](https://docs.google.com/document/d/15iH3EBfDCNRt03zOzu84dXC3u-kBq6fpOj4dIB6rJGA/edit) as closed and won in SalesForce.

* Step 2: Upon closing of opportunity as “Closed and won” in SalesForce, the Billing Team sends the quote to Zuora leading to creation of customer subscription in Zuora. Once a customer subscription is created in Zuora, Zuora automatically pushes the [customer details](https://docs.google.com/document/d/1Vauh36fizeaV7doPhftCGmpdMwZoUkn-p9xxMRTH-fM/edit) to Customer portal. 

* Step 3: Upon successful receipt of Zuora reference number the customer portal will push the [details to the license application](https://docs.google.com/document/d/1QDOfqrcuAteeLukTk0VGyeRzVXH66WjiYdx0psFQ2Cg/edit). Details such as customer info, contact details and product details are automatically pushed into the license application.

* Step 4: license application automatically sends license Key to customer through an email, alternatively customer can also download the license key through customer portal.



**c. Reseller assisted Sales:**

GitLab reseller is involved in the sales process, Sales representative uses SalesForce to create customer accounts, send out quotes and close the deal

* Step 1: At the time of updating an opportunity for a reseller account, End user name will be updated under [“Sold to Contact”](https://docs.google.com/document/d/14c6dQ-d7-8jSNOj7WdnJRoUwrBMilHEzsHoJF7HVRlQ/edit) (Based on this update, license details will be shared with the end user.

* Step 2: End user’s acceptance of licensing agreement to be updated as required (Field [“Click through EULA required”](https://docs.google.com/document/d/1y-kOeaSDQS2ZRhV-eOmloO46iUO5mHUaWlW7SqTDtbY/edit) to be updated as “Yes”).

* Step 3: End customer will receive an email to accept the terms and conditions. The opportunity can move to the next step only upon acceptance of terms and conditions by End customers. 

* Step 4: Sales team completes the sales and marks the [opportunity](https://docs.google.com/document/d/1bMj3wDX860kGk1ZxVvB721rls7VIAAlPBRpo9xcgMU4/edit) as closed and won in SalesForce.

* Step 5: Upon closing of opportunity as “Closed and won” SalesForce the Billing Team  sends the quote to Zuora leading to creation of customer subscription in Zuora. Once a customer subscription is created in Zuora, Zuora automatically pushes the customer details to [Customer portal](https://docs.google.com/document/d/1Se_JzQrg19USGdYiIjB3I-En1yyq1JOMrIXHNDrDEH0/edit). 

* Step 6: Upon successful receipt of Zuora reference number and acceptance of EULA by customer the customer portal will push the details to the [license application](https://docs.google.com/document/d/1RxDYzXgOrCBstF_DQzDJtEJDX2SZ0PUkDkFVqYN-LKY/edit) . Details such as customer info, contact details and product details are automatically pushed into the license application.

* Step 7: license application automatically sends license Key to customer through an email, alternatively customer can also download the license key through customer portal.



**Manual generation and dispatch of Duplicate License Key**

Broadly there are three circumstances in which License keys needs to be generated and dispatched manually:<br>

         
1.  Customer faces difficulty in accessing the license key and raises a ticket via Zendesk.<br>

    * Step 1: Ticket is received in Zendesk (User raises the [ticket](https://docs.google.com/document/d/1hkvV72IDzKB5QXD0BIWZWtvIeQ-dHCIWwyZG7nogR4Q/edit) for help).

    * Step 2: Based on the problem the Support Engineer decides how the problem is to be solved and [whether a duplicate license key is required](https://docs.google.com/document/d/1OO7Fi2Re2ol7mipu_RVbhtkrqWlgDSt6WKDluKLSjHU/edit). 

    * Step 3: If a duplicate license key is required, Support Engineer will generate a duplicate license key by clicking on “Duplicate licenses” under the [Edit licenses tab](https://docs.google.com/document/d/1aehHB8cTT0VY0k4bK-_T4bzgAJzRt-dpC-wvWQY8MR0/edit).

    * Step 4 : Support Engineer enters [details of number of users](https://docs.google.com/document/d/1hQbtXvgEX__Jqeu2wIP3A6VN5I0lh_uZG0gkb94mcuw/edit) that require access to duplicate licenses. 

    * Step 5: Support Engineer to reference the Zendesk ticket or Internal GitLab issue and enter any comments and notes required in the [“Notes” section](https://docs.google.com/document/d/1xj4SBCj0_XSRkO5CoIi6Z8e56_w8cDgVv7vT3w9PNTY/edit).

    * Step 6: To generate the duplicate license Key, the support engineer clicks on [“Create license”](https://docs.google.com/document/d/1qjwqgBdWkEXHtORiu9q7itf4qQpVEykDv22TfHv9SYc/edit).


2.  Sales team offers trial license keys to Prospective/ Current customers.<br>
3.  Sales team decides to issue the license manually  (In these cases, the billing team marks the customer’s profile as “Silent” in Zuora. This causes the licence to not generate automatically).<br>






**Subscription Cancellations:**

Customers can cancel the subscription anytime within 45 days and avail full refund for the amount paid. Based on customer requests the billing team cancels the subscription and invoice  in zuora and creates an opportunity with a negative value in Salesforce.

***Controls being implemented: QTC.C.25, QTC.C.44, QTC.C.47, QTC.C.48, QTC.C.42, QTC.C.43, QTC.C.45, QTC.C.46, QTC.C.49 & QTC.C.53***

<br>


**2. On Cloud  Model**

In the on cloud model there is no concept of Licence Keys, the successful provisioning of an account will automatically enable the customer to start using the account.<br>
Once a purchase has been made the customer has to connect his/her customer account (Customer Portal) with GitLab.com by opening both the tabs simultaneously and selecting the connect option available in the customer portal. 


**a. Web Direct Sales:**

Customer logs in to Customers Portal and directly buys the product as per his/her requirements.


* Step 1: Customer logs on to the Customer Portal and  clicks on [“Buy new subscription”](https://docs.google.com/document/d/1rMWfTtj76ye3xuNGqgjBvItEQsUwJhDwLHot0bnbi1o/edit) to select the subscription needed. If the customer is an already existing customer then the system [prompts](https://docs.google.com/document/d/1jsnXvmMNiYTlkO_Xro2U4mEN5_wC5Qi1O4VPhtHpuxE/edit) if the customer wants to manage an existing account or add a new subscription.

* Step 2: Customer selects the subscription that clicks on [“Order Plan”.](https://docs.google.com/document/d/1PNSXqrKWABLit63V-OVdztQf4vymmBvDTRJJOofS5WI/edit)

* Step 3: Customers need to [connect](https://docs.google.com/document/d/1qjIsTZjOYKsbFCAudrP5Hz_VEAuVqMJbtr4tzJWOjJc/edit) their account to GitLab.com account. Once the account is connected to GitLab.com, the system automatically calculates the number of existing users in case of an upgrade.

* Step 4: Once the purchase is completed, the system automatically provisions the account by [updating the plan](https://docs.google.com/document/d/1FY3DiHg8_DHN2XnQ3JjTL1XmNw4bGbVRIuYALBNuWw8/edit) in the user account. 



**b. Sales team assisted sales:**

Sales team is involved in the sales process, SalesForce is used to create customer accounts, send out quotes and close the deal.

* Step 1: Sales team completes the sales and[ marks the opportunity](https://docs.google.com/document/d/1rH4-Uk0GI71Cc-QUVIVufCRUXX6ohN1nZ0_hAdvQsLM/edit) as closed and won in SalesForce.
 
* Step 2: Upon closing of opportunity as “Closed and won” in SalesForce, the Billing Team  sends the quote to Zuora leading to creation of customer subscription in Zuora. Once a customer subscription is created in Zuora, Zuora automatically pushes the customer [details to Customer portal](https://docs.google.com/document/d/15uwg8uDjfylopuh3CuNnRGAOOEN6_GCf57KLLfKj93s/edit). 

* Step 3: Customers need to [link](https://docs.google.com/document/d/1aBqXQppzo99Uoa91q1Y7x9q9MYt91PrPWFpqiVEOBqA/edit) the GitLab customer portal with their account in GitLab.com. Click on the “My accounts” option in Customer portal and select account details option.

* Step 4: Click on [Link Account/Change linked account](https://docs.google.com/document/d/1mtega3-recCDwl7UvgSt4Cywd5jwRkvShS_mvLObgOE/edit) option.

* Step 5: Log on to a different browser tab and log in to GitLab.com. Come back to Customer portal and Click on [“Change linked account/Link account”](https://docs.google.com/document/d/1xtp_m_ZWxxwKYrPcPz-bkicDtTWHl-hulBKMNYFQOzc/edit) to connect both the accounts. There is an API Cache that has the login details, once the “Change linked account/Link account” button is selected the accounts get linked.

* Step 6: Now the customer needs to [link the name space](https://docs.google.com/document/d/1zaC76xddppTwwT-MoMQmilp2FSZSRCQZF3fRwEZhjOY/edit). Because on GitLab.com a customer can have unlimited number of groups and the customer can link his/her subscription to any one of the groups or your personal namespace. This process basically indicates to the system as to where the subscription is applied to.

* Step 7: Select “[Change linked namespace](https://docs.google.com/document/d/1XwAKdUpNbcFKYHl5X3JqjhXEYg4-xeYyH3pXzQJGkU8/edit)” and come into the namespace by clicking on the namespace.

* Step 8: Select the subscription and click on “[Processed to checkout](https://docs.google.com/document/d/18-fSatW1vRMck-W1hlohrp-f_Vw_nsvJ6-oVsMXM7FY/edit)” to apply the selection.

* Step 9: Once “Proceed to check out” is selected the system [reads the number of users](https://docs.google.com/document/d/1mzBy_ncsWLeBzaoM-RWEYUcvVb65bTd3pGpmjz4FqSA/edit) in that particular group or namespace and if it was more than the seats that were sold then the system will walk the customer through the payment for overage. 


**Subscription Cancellations:**

**Premature cancellation: Cancellation within the first 45 days.**

Ideally once the billing team cancels the subscription in Zuora, Zuora creates an amended invoice and based on the amended invoice customer portal instructs GitLab.com to downgrade the customer account to the basic version.  
Since this is not happening automatically, GitLab admin downgrades the account manually.

* Step 1: Billing team creates an internal [issue](https://docs.google.com/document/d/1UrAMTI53Qj5XvvFi4Q-BZnkKJhqkbvkYZ2XKQ62ri1g/edit) for the support team to have the subscription cancelled manually.

* Step 2: Support admin goes into the actual customer account and select the “[Edit](https://docs.google.com/document/d/1oSHzf4j54Ak_lrSeEBAFcuaqcY-yRMtFMkfXLBX0Eyw/edit)” button.
 
* Step 3: And [change the existing plan](https://docs.google.com/document/d/17P10gzJfNJPNxjgduzNXJebCli36y085N_9t6x3IMUM/edit) i.e “Gold” to “No Plan”.

* Step 4: Update the [pipeline minutes](https://docs.google.com/document/d/13JI525IrqJZCJRb6TmpMW88e110OBud3nS4cgXruGNE/edit) to reflect the pipeline minutes of the downgraded plan.

* Step 5: Enter a [note](https://docs.google.com/document/d/1tGInLoXsgEuoPQFalI6XrSOTcOD8joYmf61gotYjbzM/edit) to maintain details of why the change was made and provide a link to the issue created by the billing team requesting the changes. A note can only be entered when cancelling a user account, when cancelling a group account, there is no admin note feature. 

* Step 6: Click on “[Save changes](https://docs.google.com/document/d/1FlGUy8Sq0qs0dS3Wq8EkbcpLHtzlNnMFH1FOTrvBbFU/edit)” to give effect to the changes.


**Cancellations due to failure to renew the subscription by customer:**

Due to system limitations currently the cancellation/downgrades of the customer account from paid subscription to “Free Plan” does not happen. This has resulted in multiple accounts continuing to have access to paid features even after expiration of the subscription period. However the support services for these accounts are shut down.<br>
 
 

**c. Reseller assisted Sales**

GitLab reseller is involved in the sales process, Sales representative uses SalesForce to create customer accounts, send out quotes and close the deal.

* Step 1: At the time of updating an opportunity for a reseller account, End user name will be updated under “[Sold to Contact”](https://docs.google.com/document/d/1T7RPmjXT8CVpQqticVpRTIfFJWMc1vNC_Hqrcv6FCwg/edit) (Based on this update, license details will be shared with the end user.

* Step 2: Sales team completes the sales and [marks the opportunity](https://docs.google.com/document/d/1LITdgrQKqFnhD5ApkoFVhrsl3T1ADEV5XYa7ypFDg7o/edit) as closed and won in SalesForce.

* Step 3: End customer will receive an email to accept the terms and conditions. The opportunity can move to the next step only upon acceptance of terms and conditions by End customers. 

* Step 4: End user’s acceptance of licensing agreement to be updated as required (Field [“Click through EULA required”](https://docs.google.com/document/d/1_e8ki2rfqvWyGNGmEt3sVr9A-OOQP_gZJN83rziN1mI/edit) to be updated as “Yes”).

* Step 5: Sales team completes the sales and [marks](https://docs.google.com/document/d/1jztd8LPmHGaaOdzeRcAZeePeDU8u85v-Uh2nF3BTbpc/edit) the opportunity as closed and won in SalesForce.

* Step 6: Upon closing of opportunity as “Closed and won” SalesForce the Billing Team  sends the quote to Zuora leading to creation of customer subscription in Zuora. Once a customer subscription is created in Zuora, Zuora automatically pushes the [customer details to Customer portal.](https://docs.google.com/document/d/1VVnigNSfMDmdP5S8bMiZ-vyqDRPVyp_gH9BbEEW6-IU/edit) 

* Step 7: Customers need to [link the GitLab customer portal](https://docs.google.com/document/d/1Zg_JgmMotSXdXRWoZsHN5AsRtLyxJ112p0D1-apNv8o/edit) with their account in GitLab.com. Click on the “My accounts” option in Customer portal and select account details option.

* Step 8: Click on [Link Account/Change linked account option](https://docs.google.com/document/d/1EFPxlulStwdArlcozhC2qCr19oiiyPknIn20s1Lc1FA/edit).

* Step 9: Log on to a different browser tab and log in to GitLab.com. Come back to Customer portal and Click on “Change linked account/Link account” to connect both the accounts. There is an API Cache that has the login details, once the [“Change linked account/Link account”](https://docs.google.com/document/d/1irrKsrhnnVtU3Qcl5-bkqxKReqIIZShWB1DPhrNiKW0/edit) button is selected the accounts get linked.

* Step 10: Now the customer needs to [link the name space](https://docs.google.com/document/d/1M3ZHmri2s0njoZl9jelITpG-WxT6X8Jlwp_dXZNnTo8/edit?usp=drive_web&ouid=111314924466435551734). Because on GitLab.com a customer can have unlimited number of groups and the customer can link his/her subscription to any one of the groups or your personal namespace. This process basically indicates to the system as to where the subscription is applied to.

* Step 11: Select “[Change linked namespace](https://docs.google.com/document/d/1i-mpZY4EQer5QWoims6eEItRgZe9mp0Pek5edIHxWjM/edit)” and come into the namespace by clicking on the namespace.

* Step 12: Select the subscription and click on “[Processed to checkout](https://docs.google.com/document/d/1Z4CJTx-tqeINDYCEIGhZQxrMcM4qYIot-sTiYNlFZl0/edit)” to apply the selection.

* Step 13: Once “Proceed to check out” is selected the system [reads the number of users](https://docs.google.com/document/d/1GmnFDhg70h6B5RO602FBU4B_-AlrJZMEj5mFTZAjPiQ/edit) in that particular group or namespace and if it was more than the seats that were sold then the system will walk the customer through the payment for overage. 



**Manual Provisioning of Account:**

In case the system does not automatically provision the purchase/upgrade, or due to issues linked to credit card purchases not going through, Support Engineers with administrative access can manually change the plan. 

* Step 1: Support Engineers [logs in](https://docs.google.com/document/d/1aKHB4m3aHSUeyNw1FSyQDf34H1haUAPQGD12P1uFS-E/edit) using administrative access. [Administrative view](https://docs.google.com/document/d/17LsiyS8k6qo4N6qYuy3S5yxHBbXM8Jr4C-IEPHCH9mo/edit)

* Step 2: Click on the “[Edit](https://docs.google.com/document/d/1RPR3htkBhGqI5X_Ee4TFEopd_eM2pZWMiaHDYlF8Pso/edit)” button to make the changes.

* Step3: Make the [changes](https://docs.google.com/document/d/1wQxC8eWZc2tQ1ggncy1G5l_-Mn7Oxz7EC_6lVRW-E_8/edit) to Plan.

* Step 4: Add a [note](https://docs.google.com/document/d/1fTSdUO-8yivWAtqhz5w-1zU7PuIs9qV4xWDXn4qmKT4/edit) justifying the reason for change and link to Zen Desk ticket raised by the customer.

* Step 5: Click on “[save changes](https://docs.google.com/document/d/1qKa9m4FUe_TnfgqZCahPEPE9VkQKCERIP6pa0e1q0kA/edit)” to complete the change.

***Controls being implemented: QTC.C.50, QTC.C.51 & QTC.C.52***



